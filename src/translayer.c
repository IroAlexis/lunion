/*
 * translayer.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "translayer.h"

#include "utils/debug.h"
#include "utils/memory.h"
#include "arch.h"
#include "parser.h"
#include "utils.h"

#include <assert.h>
#include <glib/gstdio.h>
#include <fcntl.h>
#include <gio/gio.h>



/*
 * TODO Need refactor code and using new style static functions
 */

LN_DEFAULT_DEBUG_CHANNEL(translayer);


static char* dxvk_dlls[] =        {"d3d9", "d3d10core", "d3d11", "dxgi", NULL};
static char* vkd3dproton_dlls[] = {"d3d12", "d3d12core", NULL};

static char*   ln_translayer_build_path_dll_backup (const char* path, const char* dll);
static bool ln_translayer_create_fake_dll          (const char* dll);
static char*   ln_translayer_get_lib_arch          (const char* path, LnTranslayer type, LnArch arch, bool is_integrated);
static bool ln_translayer_move_dll                 (const char* src, const char* dest);



static char* _build_cache_path(LnTranslayer type, const char* path)
{
	assert(path);

	char* dirname = NULL;
	char* cc_path = NULL;

	if (type == DXVK)
	{
		dirname = g_strndup("dxvk_state_cache", 16);
	}
	if (type == VKD3DPROTON)
	{
		dirname = g_strndup("vkd3d_proton_shader_cache", 25);
	}

	if (dirname)
	{
		cc_path = g_build_path(G_DIR_SEPARATOR_S, path, dirname, NULL);

		ln_free(dirname);
	}

	return cc_path;
}


static char* _enum_to_string(LnTranslayer type)
{
	char* str = NULL;

	if (type == DXVK)
	{
		str = g_strndup("dxvk", 4);
	}
	if (type == VKD3DPROTON)
	{
		str = g_strndup("vkd3dproton", 11);
	}

	return str;
}


static char* ln_translayer_convert_arch(LnTranslayer type, LnArch arch, bool is_integrated)
{
	char* ret = NULL;

	if (is_integrated)
	{
		if (arch == LN_ARCH_64BIT_MODE)
		{
			ret = g_strndup("x86_64", 6);
		}
		if (arch == LN_ARCH_32BIT_MODE)
		{
			ret = g_strndup("x86", 3);
		}
	}
	else
	{
		if (arch == LN_ARCH_64BIT_MODE)
		{
			ret = g_strndup("x64", 3);
		}
		if (arch == LN_ARCH_32BIT_MODE && type == DXVK)
		{
			ret = g_strndup("x32", 3);
		}
		if (arch == LN_ARCH_32BIT_MODE && type == VKD3DPROTON)
		{
			ret = g_strndup("x86", 3);
		}
	}

	return ret;
}



static bool ln_translayer_backup_dll(const char* path)
{
	assert(path);

	char* backup = NULL;
	bool is_done = false;
	bool status = false;

	backup = ln_translayer_build_path_dll_backup(path, NULL);
	if (backup && ! (g_file_test(path, G_FILE_TEST_EXISTS) && g_file_test(backup, G_FILE_TEST_EXISTS)))
	{
		// dll isn't' present into wine prefix, so create fake dll ".old_none"
		if (! g_file_test(path, G_FILE_TEST_EXISTS))
		{
			is_done = ln_translayer_create_fake_dll(backup);
		}
		// dll is present into wine prefix, so rename this dll into ".old"
		else
		{
			is_done = ln_translayer_move_dll(path, backup);
		}
	}

	if (is_done && g_file_test(path, G_FILE_TEST_EXISTS) && g_file_test(backup, G_FILE_TEST_EXISTS))
	{
		status = true;
	}

	ln_free(backup);

	return status;
}


static char* ln_translayer_build_path_dll(const char* path, const char* dll)
{
	assert(path);
	assert(dll);

	char* tmp = NULL;
	char* filepath = NULL;

	if ((tmp = g_build_path("/", path, dll, NULL)))
	{
		filepath = g_strconcat(tmp, ".dll", NULL);

		ln_free(tmp);
	}

	return filepath;
}


static char* ln_translayer_build_path_dll_backup(const char* path, const char* dll)
{
	assert(path);

	char* tmp = NULL;
	char* ret = NULL;

	if (! g_str_has_suffix(path, ".dll"))
	{
		tmp = ln_translayer_build_path_dll(path, dll);
	}
	else
	{
		tmp = g_strdup(path);
	}

	if (tmp)
	{
		ret = g_strconcat(tmp, ".old", NULL);
		if (! g_file_test(tmp, G_FILE_TEST_EXISTS))
		{
			char* p_tmp = NULL;

			p_tmp = g_strdup(ret);
			ln_free(ret);
			ret = g_strconcat(p_tmp, "_none", NULL);

			ln_free(p_tmp);
		}

		ln_free(tmp);
	}

	return ret;
}


static bool ln_translayer_create_fake_dll(const char* dll)
{
	assert(dll);

	GFile* file = NULL;
	GFileOutputStream* io_stream = NULL;
	bool status = false;

	if ((file = g_file_new_for_path(dll)))
	{
		GError* error = NULL;

		if ((io_stream = g_file_create(file, G_FILE_CREATE_NONE, NULL, &error)))
		{
			status = true;
			TRACE("-> %s", dll);

			g_object_unref(io_stream);
			io_stream = NULL;
		}

		if (error)
		{
			g_error_free(error);
			error = NULL;
		}

		g_object_unref(file);
		file = NULL;
	}

	return status;
}


static char* ln_translayer_get_key(LnTranslayer type)
{
	char* ret = NULL;

	switch(type)
	{
		case DXVK:
			ret = g_strndup("dxvk", 4);
			break;
		case VKD3DPROTON:
			ret = g_strndup("vkd3d_proton", 12);
			break;
	}

	return ret;
}


static LnLib* ln_translayer_get_lib(LnTranslayer type, GKeyFile* cfg, const char* program_id)
{
	assert(program_id);

	bool is_int = false;
	char* key = NULL;
	char* path = NULL;
	LnLib* ret = NULL;

	if ((key = ln_translayer_get_key(type)))
	{
		if ((path = ln_parser_get_string(cfg, program_id, key)))
		{
			char* path64 = NULL;
			char* path32 = NULL;

			path64 = ln_translayer_get_lib_arch(path, type, LN_ARCH_64BIT_MODE, is_int);
			path32 = ln_translayer_get_lib_arch(path, type, LN_ARCH_32BIT_MODE, is_int);

			if (path64 || path32)
			{
				ret = ln_lib_new(path32, path64);
			}

			ln_free(path64);
			ln_free(path32);
			ln_free(path);
		}

		ln_free(key);
	}

	return ret;
}


static char* ln_translayer_get_lib_arch(const char* path, LnTranslayer type, LnArch arch, bool is_integrated)
{
	assert(path);

	char* _arch = NULL;
	char* ret = NULL;

	if ((_arch = ln_translayer_convert_arch(type, arch, is_integrated)))
	{
		ret = g_build_path("/", path, _arch, NULL);
		if (ret && ! g_file_test(ret, G_FILE_TEST_IS_DIR))
		{
			ln_free(ret);
		}

		ln_free(_arch);
	}

	return ret;
}


static bool ln_translayer_install_dll(const char* src, const char* dest)
{
	assert(src);
	assert(dest);

	GFile* source = NULL;
	GFile* destination = NULL;
	bool status = false;

	source = g_file_new_for_path(src);
	destination = g_file_new_for_path(dest);

	if (source && destination)
	{
		GError* error = NULL;

		if ((status = g_file_copy(source, destination, G_FILE_COPY_OVERWRITE, NULL, NULL, NULL, &error)))
		{
			TRACE("%s -> %s", src, dest);
		}

		if (error)
		{
			g_error_free(error);
			error = NULL;
		}
	}

	if (source)
	{
		g_object_unref(source);
		source = NULL;
	}
	if (destination)
	{
		g_object_unref(destination);
		destination = NULL;
	}

	return status;
}


static bool ln_translayer_install_lib(LnTranslayer type, const char* src_path, const char* wine_path)
{
	assert(src_path);
	assert(wine_path);

	char** dlls = NULL;
	int nb_dlls = 0;
	int count = 0;
	bool status = false;

	switch(type)
	{
		case DXVK:
			dlls = dxvk_dlls;
			nb_dlls = 4;
			break;
		case VKD3DPROTON:
			dlls = vkd3dproton_dlls;
			nb_dlls = 1;
			break;
	}

	for (char** tmp = dlls; *tmp; tmp++)
	{
		char* src = ln_translayer_build_path_dll(src_path, *tmp);
		char* dest = ln_translayer_build_path_dll(wine_path, *tmp);

		if (src && dest)
		{
			ln_translayer_backup_dll(dest);
			ln_translayer_install_dll(src, dest);
			count++;
		}

		ln_free(src);
		ln_free(dest);
	}

	if (count == nb_dlls)
	{
		status = true;
	}

	return status;
}


static bool ln_translayer_move_dll(const char* src, const char* dest)
{
	assert(src);
	assert(dest);

	GFile* source = NULL;
	GFile* destination = NULL;
	bool status = false;

	source = g_file_new_for_path(src);
	destination = g_file_new_for_path(dest);

	if (source && destination)
	{
		GError* error = NULL;

		if ((status = g_file_move(source, destination, G_FILE_COPY_OVERWRITE, NULL, NULL, NULL, &error)))
		{
			TRACE("%s -> %s", src, dest);
		}

		if (error)
		{
			g_error_free(error);
			error = NULL;
		}
	}

	if (source)
	{
		g_object_unref(source);
		source = NULL;
	}
	if (destination)
	{
		g_object_unref(destination);
		destination = NULL;
	}

	return status;
}


/* TODO Need rework this function and pass to be public function
static bool ln_translayer_test_dll(const char* path, const char** dlls)
{
	assert(path);
	assert(dlls);

	bool ret = true;

	for (const char** tmp = dlls; *tmp; tmp++)
	{
		char* dll = NULL;
		char* old = NULL;

		dll = g_build_path("/", path, *tmp, NULL);
		old = g_strconcat(dll, ".old", NULL);

		ret = ret && (g_file_test(path, G_FILE_TEST_EXISTS) &&
			g_file_test(old, G_FILE_TEST_EXISTS));
	}

	return ret;
}
*/


static void ln_translayer_setup_dxvk_runtime(const char* path)
{
	if (g_getenv("LUNION_LOG") == NULL)
	{
		g_setenv("DXVK_LOG_LEVEL", "none", false);
	}
	TRACE("%s=%s", "DXVK_LOG_LEVEL", g_getenv("DXVK_LOG_LEVEL"));

	if (path)
	{
		g_setenv("DXVK_STATE_CACHE_PATH", path, false);
		TRACE("DXVK_STATE_CACHE_PATH=%s", g_getenv("DXVK_STATE_CACHE_PATH"));
	}
}


static void ln_translayer_setup_vkd3d_proton_runtime(const char* path)
{
	if (g_getenv("LUNION_LOG") == NULL)
	{
		g_setenv("VKD3D_DEBUG", "none", false);
	}
	TRACE("%s=%s", "VKD3D_DEBUG", g_getenv("VKD3D_DEBUG"));

	if (path)
	{
		g_setenv("VKD3D_SHADER_CACHE_PATH", path, false);
		TRACE("VKD3D_SHADER_CACHE_PATH=%s", g_getenv("VKD3D_SHADER_CACHE_PATH"));
	}
}


LnLib* ln_translayer_get_dxvk(GKeyFile* cfg, const char* program_id)
{
	assert(program_id);

	return ln_translayer_get_lib(DXVK, cfg, program_id);
}


LnLib* ln_translayer_get_vkd3dproton(GKeyFile* cfg, const char* program_id)
{
	assert(program_id);

	return ln_translayer_get_lib(VKD3DPROTON, cfg, program_id);
}


void ln_translayer_install(const LnTranslayer type, const LnLib* src, const LnLib* dest)
{
	assert(src);
	assert(dest);

	const char* dst32 = NULL;
	const char* dst64 = NULL;

	dst64 = ln_lib_get_path(dest, LN_ARCH_64BIT_MODE);
	if (dst64)
	{
		ln_translayer_install_lib(type,
			ln_lib_get_path(src, LN_ARCH_64BIT_MODE),
			dst64);
	}

	dst32 = ln_lib_get_path(dest, LN_ARCH_32BIT_MODE);
	if (dst32)
	{
		ln_translayer_install_lib(type,
			ln_lib_get_path(src, LN_ARCH_32BIT_MODE),
			dst32);
	}
}


void ln_translayer_init(LnTranslayer type, const char* path)
{
	assert(path);

	char* cc_path = NULL;
	char* dlls = NULL;

	/* Need create manually directory */
	cc_path = _build_cache_path(type, path);
	if (! cc_path || (cc_path && g_mkdir_with_parents(cc_path, 0700) != 0))
	{
		char* type_s = NULL;

		if ((type_s = _enum_to_string(type)))
		{
			WARN("%s: Skipping cache path configuration", type_s);

			ln_free(type_s);
		}
		else
		{
			WARN("Skipping translayer cache path configuration");
		}
	}

	if (type == DXVK)
	{
		ln_translayer_setup_dxvk_runtime(cc_path);
		dlls = g_strndup("d3d9,d3d10core,d3d11,dxgi=n", 27);
	}
	if (type == VKD3DPROTON)
	{
		ln_translayer_setup_vkd3d_proton_runtime(cc_path);
		dlls = g_strndup("d3d12,d3d12core=n", 17);
	}

	ln_append_env("WINEDLLOVERRIDES", dlls, ";");
	TRACE("%s=%s", "WINEDLLOVERRIDES", g_getenv("WINEDLLOVERRIDES"));

	ln_free(cc_path);
	ln_free(dlls);
}
