/*
 * lib.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "lib.h"

#include "utils/debug.h"
#include "utils/memory.h"

#include <assert.h>



LN_DEFAULT_DEBUG_CHANNEL(lib);


struct _ln_lib
{
	char* path64;
	char* path32;
};



void ln_lib_unref(LnLib** self)
{
	if (*self)
	{
		ln_free((*self)->path64);
		ln_free((*self)->path32);
		ln_free(*self);
	}
}


const char* ln_lib_get_path(const LnLib* self, LnArch arch)
{
	const char* ret = NULL;

	if (arch == LN_ARCH_64BIT_MODE)
	{
		ret = self->path64;
	}

	if (arch == LN_ARCH_32BIT_MODE)
	{
		ret = self->path32;
	}

	return ret;
}


LnLib* ln_lib_new(const char* path32, const char* path64)
{
	LnLib* self = NULL;

	self = (LnLib*) ln_calloc(1, sizeof(LnLib));

	if (path32)
	{
		self->path32 = g_strdup(path32);
	}

	if (path64)
	{
		self->path64 = g_strdup(path64);
	}

	return self;
}


void ln_lib_setup_env(const char* key, const LnLib* self)
{
	assert (key);
	assert (self);

	if (self->path64)
	{
		ln_append_env(key, self->path64, ":");
	}
	if (self->path32)
	{
		ln_append_env(key, self->path32, ":");
	}

	TRACE("%s=%s", key, g_getenv(key));
}
