/*
 * debug.h
 *
 * Copyright (C) 2024 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __DEBUG__
#define __DEBUG__

#include <stdio.h>


/*
 * Private definition don't use directly
 */
typedef enum _ln_debug_class
{
	LN_DBG_CLASS_INFO    = 0x0,
	LN_DBG_CLASS_ERROR   = 0x1,
	LN_DBG_CLASS_WARNING = 0x2,
	LN_DBG_CLASS_TRACE   = 0x3,
} _LnDebugClass;


typedef struct _ln_debug_channel
{
	char name[16];
} _LnDebugChannel;


#define LN_DECLARE_DEBUG_CHANNEL(ch) \
	static const _LnDebugChannel dbch_##ch = { #ch }
#define LN_DEFAULT_DEBUG_CHANNEL(ch) \
	LN_DECLARE_DEBUG_CHANNEL(ch); \
	static const _LnDebugChannel* dbch_default = &dbch_##ch

#define _LN_DBG_FPRINTF(cls, ...) ln_debug_fprintf(LN_DBG_CLASS_##cls, __VA_ARGS__)

/*
 * Public definition
 */

#define INFO(...)       _LN_DBG_FPRINTF(INFO, dbch_default, __VA_ARGS__)
#define INFO_(ch, ...)  _LN_DBG_FPRINTF(INFO, &dbch_##ch,   __VA_ARGS__)

#define ERR(...)        _LN_DBG_FPRINTF(ERROR, dbch_default, __VA_ARGS__)
#define ERR_(ch, ...)   _LN_DBG_FPRINTF(ERROR, &dbch_##ch,   __VA_ARGS__)

#define WARN(...)       _LN_DBG_FPRINTF(WARNING, dbch_default, __VA_ARGS__)
#define WARN_(ch, ...)  _LN_DBG_FPRINTF(WARNING, &dbch_##ch,   __VA_ARGS__)

#define TRACE(...)      _LN_DBG_FPRINTF(TRACE, dbch_default, __VA_ARGS__)
#define TRACE_(ch, ...) _LN_DBG_FPRINTF(TRACE, &dbch_##ch,   __VA_ARGS__)



void ln_debug_fprintf(const _LnDebugClass dbcl, const _LnDebugChannel* dbch, const char* format, ...);

void ln_debug_set_class_max(const _LnDebugClass dbcl);



#endif
