/*
 * memory.c
 *
 * Copyright (C) 2024 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



#include "memory.h"
#include "debug.h"


LN_DEFAULT_DEBUG_CHANNEL(memory);


void* ln_calloc(size_t count, size_t size)
{
	void* ptr = NULL;

	ptr = calloc(count, size);
	if (NULL == ptr)
	{
		ERR("Out of memory");
	}

	return ptr;
}
