/*
 * translayer.h
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __TRANSLAYER__
#define __TRANSLAYER__


#include "lib.h"

#include <glib.h>



typedef enum
{
	DXVK,
	VKD3DPROTON
} LnTranslayer;


LnLib* ln_translayer_get_dxvk(GKeyFile* cfg, const char* program_id);

LnLib* ln_translayer_get_vkd3dproton(GKeyFile* cfg, const char* program_id);

void ln_translayer_install(const LnTranslayer, const LnLib* src, const LnLib* dest);

void ln_translayer_init(LnTranslayer type, const char* program_cache_path);



#endif
