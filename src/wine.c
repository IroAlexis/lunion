/*
 * wine.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "wine.h"

#include "utils/debug.h"
#include "utils/memory.h"
#include "arch.h"
#include "utils.h"

#include <assert.h>
#include <glib/gstdio.h>
#include <string.h>



LN_DEFAULT_DEBUG_CHANNEL(wine);


struct _ln_wine
{
	LnArch bin;
	LnArch bin64;
	char* path;
};



static LnArch _get_executable_arch(const char* path, const char* name);

static void _init_debug(void);

static bool _is_valid(const char* path);

static bool _is_wow64(const LnWine* self);

static bool _test_executable(const char* path, const char* filename);


static LnArch _get_executable_arch(const char* path, const char* name)
{
	assert(path);
	assert(name);

	char* exec = NULL;
	LnArch arch = LN_ARCH_NULL;

	if ((exec = g_build_filename(path, name, NULL)))
	{
		if (g_file_test(exec, G_FILE_TEST_IS_EXECUTABLE) && ! g_file_test(exec , G_FILE_TEST_IS_SYMLINK))
		{
			arch = ln_arch_get_from_binary_file(exec);
		}

		ln_free(exec);
	}

	return arch;
}


static void _init_debug(void)
{
	if (! getenv("WINEDEBUG"))
	{
		char* ch = NULL;

		if (ln_get_debug_mode() == 0)
		{
			ch = strndup("-all", strlen("-all"));
		}
		else
		{
			ch = strndup("fixme-all", strlen("fixme-all"));
		}

		g_setenv("WINEDEBUG", ch, false);

		ln_free(ch);
	}
}


static bool _is_valid(const char* path)
{
	assert(path);

	bool status = false;

	if (g_file_test(path, G_FILE_TEST_IS_DIR))
	{
		status = _test_executable(path, "wineboot") && _test_executable(path, "wineserver");
	}
	else
	{
		ERR("Not a valid directory: %s", path);
	}

	return status;
}


static bool _is_wow64(const LnWine* self)
{
	assert(self);

	bool status = false;

	/* Experimental wow64 mode since 8.0+ */
	if (self->bin == LN_ARCH_64BIT_MODE && self->bin64 == LN_ARCH_NULL)
	{
		status = true;
	}

	/* Old wow64 mode */
	if (self->bin == LN_ARCH_32BIT_MODE && self->bin64 == LN_ARCH_64BIT_MODE)
	{
		status = true;
	}

	return status;
}


static bool _test_executable(const char* path, const char* filename)
{
	assert(path);
	assert(filename);

	char* exec = NULL;
	bool status = false;

	if ((exec = g_build_filename(path, filename, NULL)))
	{
		status = g_file_test(exec, G_FILE_TEST_IS_EXECUTABLE);

		ln_free(exec);
	}

	if (! status)
	{
		ERR("%s: No such executable", exec);
	}

	return status;
}


/* TODO Need rework this functions for don't depend to GKeyFile
static void ln_wine_use_debug(GKeyFile* cfg, const char* group)
{
	assert(group);

	char* debug = NULL;

	debug = ln_parser_get_string(cfg, group, "use_winedbg");
	if (debug && ! g_strcmp0(debug, "true"))
	{
		ln_append_env("WINEDLLOVERRIDES", "winedebug.exe=d", ";");
	}

	ln_free(debug);
}
*/


/* TODO Need rework this functions for don't depend to GKeyFile
static void ln_wine_use_menubuilder(GKeyFile* cfg, const char* group)
{
	assert(group);

	char* symlink = NULL;

	symlink = ln_parser_get_string(cfg, group, "use_winemenubuilder");
	if (symlink && ! g_strcmp0(symlink, "true"))
	{
		ln_append_env("WINEDLLOVERRIDES", "winemenubuilder.exe=d", ";");
	}

	ln_free(symlink);
}
*/


static LnWine* ln_wine_new(const char* path, LnArch bin, LnArch bin64)
{
	assert(path);

	LnWine* self = NULL;

	if ((self = (LnWine*) ln_calloc(1, sizeof(LnWine))))
	{
		self->bin = bin;
		self->bin64 = bin64;
		self->path = g_strndup(path, (gsize) strlen(path));
	}

	return self;
}


/* Factory method */
LnWine* ln_wine_create(const char* path)
{
	assert(path);

	LnWine* self = NULL;

	TRACE("Using path: %s", path);
	if (_is_valid(path))
	{
		LnArch bin = LN_ARCH_NULL;
		LnArch bin64 = LN_ARCH_NULL;

		bin = _get_executable_arch(path, "wine");
		bin64 = _get_executable_arch(path, "wine64");

		if (bin != LN_ARCH_NULL || bin64 != LN_ARCH_NULL)
		{
			self = ln_wine_new(path, bin, bin64);
		}
	}

	return self;
}


bool ln_wine_add_registry_key(const LnWine* self, const char* key, const char* name, const char* type, const char* data)
{
	assert(self);
	assert(key);
	assert(name);
	assert(data);

	GStrvBuilder* builder = NULL;
	bool status = false;

	if ((builder = g_strv_builder_new()))
	{
		char** cmd = NULL;

		g_strv_builder_add_many(builder,
			ln_wine_get_bin_path(self), "reg", "add", key, "/v", name, "/t", type, "/d", data, "/f", NULL);

		if ((cmd = g_strv_builder_end(builder)))
		{
			status = ln_run_process(NULL , cmd, true);

			g_strfreev(cmd);
			cmd = NULL;
		}

		g_strv_builder_unref(builder);
		builder = NULL;
	}

	return status;
}


bool ln_wine_delete_registry_key(const LnWine* self, const char* key, const char* name)
{
	GStrvBuilder* builder = NULL;
	bool status = false;

	if ((builder = g_strv_builder_new()))
	{
		char** cmd = NULL;

		g_strv_builder_add_many(builder,
			ln_wine_get_bin_path(self), "reg", "delete", key, "/v", name, "/f", NULL);

		if ((cmd = g_strv_builder_end(builder)))
		{
			status = ln_run_process(NULL , cmd, true);

			g_strfreev(cmd);
			cmd= NULL;
		}

		g_strv_builder_unref(builder);
		builder = NULL;
	}

	return status;
}


void ln_wine_unref(LnWine** self)
{
	if (*self)
	{
		ln_free((*self)->path);
		ln_free(*self);
	}
}


char* ln_wine_get_bin_path(const LnWine* self)
{
	assert(self);

	char* exec = NULL;
	char* path = NULL;

	if ((exec = ln_wine_get_exec(self)))
	{
		path = g_build_filename(self->path, exec, NULL);

		ln_free(exec);
	}

	return path;
}


char* ln_wine_get_exec(const LnWine* self)
{
	assert(self);

	char* exec = NULL;

	if (_is_wow64(self) || self->bin == LN_ARCH_32BIT_MODE)
	{
		exec = g_strndup("wine", 4);
	}
	else if (self->bin == LN_ARCH_NULL && self->bin64 == LN_ARCH_64BIT_MODE)
	{
		exec = g_strndup("wine64", 6);
	}

	return exec;
}


char* ln_wine_get_registry_key(const LnWine* self, const char* key, const char* name)
{
	assert(self);
	assert(key);
	assert(name);

	g_autofree char* bin = NULL;
	char* value = NULL;

	if ((bin = ln_wine_get_bin_path(self)))
	{
		g_autofree char* cmd = NULL;

		cmd = g_strdup_printf("%s reg query \"%s\" /v \"%s\"", bin, key, name);
		if (cmd)
		{
			value = ln_get_output_cmd(cmd);

			/*
			 * TODO Parse output to get registry value
			 * Unusable in current state
			 */
		}
	}

	return value;
}


char* ln_wine_get_version(const LnWine* self)
{
	assert(self);

	char* bin = NULL;
	char* value = NULL;

	if ((bin = ln_wine_get_bin_path(self)))
	{
		char* cmd = NULL;

		cmd = g_strjoin(" ", bin, "--version", NULL);
		if (cmd && (value = ln_get_output_cmd(cmd)))
		{
			ln_remove_linefeed_to_end_string(&value);
		}

		ln_free(cmd);
		ln_free(bin);
	}
	TRACE("version: %s", value);

	return value;
}


void ln_wine_init(const LnWine* self)
{
	assert(self);

	g_setenv("WINEFSYNC", "1", false);
	/* fallback when fsync don't support */
	g_setenv("WINEESYNC", "1", false);

	ln_append_env("WINEDLLOVERRIDES", "winedbg.exe=d", ";");
	ln_append_env("WINEDLLOVERRIDES", "winemenubuilder.exe=d", ";");

	_init_debug();
	/* ln_wine_configure_gstreamer_runtime(self); */
}


bool ln_wine_make_prefix(const LnWine* self, const char* option)
{
	assert(self);

	char* dlls = NULL;
	char* dbg = NULL;
	bool status = false;

	dlls = g_strdup(g_getenv("WINEDLLOVERRIDES"));
	g_setenv("WINEDLLOVERRIDES", "mscoree,mshtml,winemenubuilder.exe=d", true);

	dbg = g_strdup(g_getenv("WINEDEBUG"));
	/* No overwrite, can be usefull for debugging */
	g_setenv("WINEDEBUG", "-all", false);

	status = ln_wine_use_boot(self, option);
	ln_wine_wait_server(self);

	ln_reset_env("WINEDLLOVERRIDES", dlls);
	ln_reset_env("WINEDEBUG", dbg);

	ln_free(dlls);
	ln_free(dbg);

	return status;
}


bool ln_wine_use_boot(const LnWine* self, const char* option)
{
	assert(self);

	char* bin = NULL;
	bool status = false;

	if ((bin = ln_wine_get_bin_path(self)))
	{
		char** cmd = NULL;

		if ((cmd = ln_build_argv(bin, "wineboot.exe", option, NULL)))
		{
			status = ln_run_process(NULL, cmd, false);

			g_strfreev(cmd);
			cmd = NULL;
		}

		ln_free(bin);
	}

	return status;
}


bool ln_wine_use_server(const LnWine* self, const char* option)
{
	assert(self);

	char* bin = NULL;
	bool status = false;

	if ((bin = g_build_filename(self->path, "wineserver", NULL)))
	{
		char** cmd = NULL;

		if ((cmd = ln_build_argv(bin, option, NULL)))
		{
			status = ln_run_process(NULL, cmd, false);

			g_strfreev(cmd);
			cmd = NULL;
		}

		ln_free(bin);
	}

	return status;
}


bool ln_wine_wait_server(const LnWine* self)
{
	assert(self);

	return ln_wine_use_server(self , "-w");
}
