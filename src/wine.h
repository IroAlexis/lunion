/*
 * wine.h
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __WINE__
#define __WINE__

#include "lib.h"

#include <glib.h>

#if defined(__STDC_VERSION__) && __STDC_VERSION__ < 202311L
#include <stdbool.h>
#endif



typedef struct _ln_wine LnWine;


bool ln_wine_add_registry_key(const LnWine* self, const char* key, const char* name, const char* type, const char* data);

LnWine* ln_wine_create(const char* path);

bool ln_wine_delete_registry_key(const LnWine* self, const char* key, const char* name);

void ln_wine_unref(LnWine** self);

char* ln_wine_get_bin_path(const LnWine* self);

char* ln_wine_get_exec(const LnWine* self);

char* ln_wine_get_version(const LnWine* self);

void ln_wine_init(const LnWine* self);

bool ln_wine_make_prefix(const LnWine* self, const char* option);

bool ln_wine_use_boot(const LnWine* self, const char* option);

bool ln_wine_use_server(const LnWine* self, const char* option);

bool ln_wine_wait_server(const LnWine* self);



#endif
