/*
 * driver.h
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __DRIVER__
#define __DRIVER__


#include "utils.h"

#include <glib.h>



/* Identical to VkDriverId (v1.3.260) */
typedef enum
{
	LN_DRIVER_NULL       = 0x0,
	LN_DRIVER_MESA_RADV  = 0x3,
	LN_DRIVER_NVIDIA     = 0x4,
	LN_DRIVER_MESA_INTEL = 0x6
} LnDriver;


LnDriver ln_driver_get_from_string(const char* string);

void ln_driver_init(const LnDriver self, const char* path);



#endif
