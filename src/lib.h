/*
 * lib.h
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __LIB__
#define __LIB__


#include "arch.h"
#include "utils.h"

#include <glib.h>



typedef struct _ln_lib LnLib;


void ln_lib_unref(LnLib** self);

const char* ln_lib_get_path(const LnLib* self, LnArch arch);

LnLib* ln_lib_new(const char* path32, const char* path64);

void ln_lib_setup_env(const char* key, const LnLib* self);



#endif
