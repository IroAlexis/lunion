/*
 * driver.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "driver.h"

#include "utils/debug.h"

#include <assert.h>



LN_DEFAULT_DEBUG_CHANNEL(driver);


static void _setenv_mesa(const char* path)
{
	assert(path);

	g_setenv("MESA_DISK_CACHE_SINGLE_FILE", "1", false);
	TRACE("MESA_DISK_CACHE_SINGLE_FILE=%s", g_getenv("MESA_DISK_CACHE_SINGLE_FILE"));

	g_setenv("MESA_SHADER_CACHE_DIR", path, false);
	TRACE("MESA_SHADER_CACHE_DIR=%s", g_getenv("MESA_SHADER_CACHE_DIR"));
}


static void _setenv_nvidia(const char* path)
{
	assert(path);

	g_setenv("__GL_SHADER_DISK_CACHE", "1", false);
	TRACE("__GL_SHADER_DISK_CACHE=%s", g_getenv("__GL_SHADER_DISK_CACHE"));

	g_setenv("__GL_SHADER_DISK_CACHE_PATH", path, false);
	TRACE("__GL_SHADER_DISK_CACHE_PATH=%s", g_getenv("__GL_SHADER_DISK_CACHE_PATH"));

	g_setenv("__GL_SHADER_DISK_CACHE_SKIP_CLEANUP", "1", false);
	TRACE("__GL_SHADER_DISK_CACHE_SKIP_CLEANUP=%s", g_getenv("__GL_SHADER_DISK_CACHE_SKIP_CLEANUP"));
}


LnDriver ln_driver_get_from_string(const char* string)
{
	assert(string);

	LnDriver self = LN_DRIVER_NULL;

	if (g_strcmp0(string , "radv") == 0)
	{
		self = LN_DRIVER_MESA_RADV;
	}
	else if (g_strcmp0(string , "nvidia") == 0)
	{
		self = LN_DRIVER_NVIDIA;
	}

	else if (g_strcmp0(string , "intel") == 0)
	{
		self = LN_DRIVER_MESA_INTEL;
	}
	else
	{
		ERR("%s: Not supported driver", string);
	}

	return self;
}


void ln_driver_init(const LnDriver self, const char* path)
{
	assert(path);

	if (self != LN_DRIVER_NULL)
	{
		if (self == LN_DRIVER_MESA_RADV || self == LN_DRIVER_MESA_INTEL)
		{
			_setenv_mesa(path);
		}

		if (self == LN_DRIVER_NVIDIA)
		{
			_setenv_nvidia(path);
		}
	}
}
