/*
 * utils.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "utils.h"

#include "utils/debug.h"
#include "utils/memory.h"

#include <assert.h>
#include <fcntl.h>
#include <libgen.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <sys/wait.h>
#include <unistd.h>



LN_DEFAULT_DEBUG_CHANNEL(utils);

enum _ln_xdg
{
	LN_XDG_NULL            = 0x0,
	LN_XDG_USER_CACHE      = 0x1,
	LN_XDG_USER_CONFIG     = 0x2,
	LN_XDG_USER_DATA       = 0x3,
	LN_XDG_USER_RUNTIME    = 0x4,
	LN_XDG_USER_STATE_FILE = 0x5
};


static char*   _get_xdg_user_default_path(enum _ln_xdg type);

static char*   _get_xdg_user_path(enum _ln_xdg type);

static char*   _ln_xdg_to_string(enum _ln_xdg value);

static void     _print_argv(char** argv);


static char* _get_xdg_user_default_path(enum _ln_xdg type)
{
	const char* home = NULL;
	char* path = NULL;

	home = g_getenv("HOME");
	switch (type)
	{
		case LN_XDG_USER_CACHE:
			path = g_build_filename(home, ".cache", NULL);
			break;
		case LN_XDG_USER_CONFIG:
			path = g_build_filename(home, ".config", NULL);
			break;
		case LN_XDG_USER_DATA:
			path = g_build_filename(home, ".local", "share", NULL);
			break;
		case LN_XDG_USER_RUNTIME:
			path = g_build_filename("tmp", NULL);
			break;
		case LN_XDG_USER_STATE_FILE:
			path = g_build_filename(home, ".local", "state", NULL);
			break;
		default:
			ERR("Not follow XDG Base Directory specification");
	}

	return path;
}


static char* _get_xdg_user_path(enum _ln_xdg type)
{
	char* key = NULL;
	char* path = NULL;

	if ((key = _ln_xdg_to_string(type)))
	{
		char* base = NULL;

		if (! (base = g_strdup(g_getenv(key))))
		{
			base = _get_xdg_user_default_path(type);
		}

		if (base)
		{
			path = g_build_filename(base, "lunion", NULL);

			ln_free(base);
		}

		ln_free(key);
	}

	return path;
}


static char* _ln_xdg_to_string(enum _ln_xdg value)
{
	char* key = NULL;

	switch (value)
	{
		case LN_XDG_USER_CACHE:
			key = g_strndup("XDG_CACHE_HOME", 14);
			break;
		case LN_XDG_USER_CONFIG:
			key = g_strndup("XDG_CONFIG_HOME", 15);
			break;
		case LN_XDG_USER_DATA:
			key = g_strndup("XDG_DATA_HOME", 13);
			break;
		case LN_XDG_USER_STATE_FILE:
			key = g_strndup("XDG_STATE_HOME", 14);
			break;
		case LN_XDG_USER_RUNTIME:
			key = g_strndup("XDG_RUNTIME_DIR", 15);
			break;
		default:
			ERR("Not follow XDG Base Directory specification");
	}

	return key;
}


static void _print_argv(char** argv)
{
	assert(argv);

	GString* msg = NULL;

	if ((msg = g_string_new(NULL)))
	{
		char** pt = argv;
		while (*pt)
		{
			g_string_append(msg, *pt);

			if (++pt)
			{
				g_string_append(msg, " ");
			}
		}

		TRACE("%s", msg->str);

		g_string_free(msg, true);
		msg = NULL;
	}
}


static void ln_insert_env(const char* name, const char* value, const char* separator, const int pos)
{
	assert(name);
	assert(value);

	const char* env = NULL;

	if ((env = g_getenv(name)))
	{
		char* tmp = NULL;

		if (pos > 0)
		{
			tmp = g_strconcat(env, separator, value, NULL);
		}
		else if (pos < 0)
		{
			tmp = g_strconcat(value, separator, env, NULL);
		}
		else
		{
			ERR("Failed to add environment variable: %s=%s", name, value);
			TRACE("%s=%s -> %d", name, value, pos);
			return;
		}

		if (tmp)
		{
			g_setenv(name, tmp, true);

			ln_free(tmp);
		}
	}
	else
	{
		g_setenv(name, value, TRUE);
	}
}


void ln_append_env(const char* name, const char* value, const char* separator)
{
	assert(name);
	assert(value);

	ln_insert_env(name, value, separator, 1);
}


char** ln_build_argv(const char* bin, const char* first_arg, ...)
{
	assert(bin);
	assert(first_arg);

	GStrvBuilder* strbuild = NULL;
	char** argv = NULL;

	if ((strbuild = g_strv_builder_new()))
	{
		char* next_arg = NULL;
		va_list args;

		g_strv_builder_add(strbuild, bin);
		g_strv_builder_add(strbuild, first_arg);

		va_start(args, first_arg);
		next_arg = va_arg(args, char*);

		while (next_arg)
		{
			g_strv_builder_add(strbuild, next_arg);
			next_arg = va_arg(args, char*);
		}

		va_end(args);
		argv = g_strv_builder_end(strbuild);

		g_strv_builder_unref(strbuild);
		strbuild = NULL;
	}

	return argv;
}


char* ln_get_basename(const char* path)
{
	char* p_path = NULL;
	char* base = NULL;

	if ((p_path = strrchr(path, '/') + 1))
	{
		base = strndup(p_path, strlen(p_path));
	}

	return base;
}


char* ln_get_compatdata_path(void)
{
	char* usrdata = NULL;
	char* path = NULL;

	if ((usrdata = ln_get_user_data_path()))
	{
		path = g_build_path(G_DIR_SEPARATOR_S, usrdata, "compatdata", NULL);

		ln_free(usrdata);
	}

	return path;
}


int ln_get_debug_mode(void)
{
	const char* p_env = NULL;
	int value = 0;

	if ((p_env = getenv("LUNION_DEBUG")))
	{
		value = (int) strtol(p_env, NULL, 10);
	}

	return value;
}


char* ln_get_dirname_bin(void)
{
	char* real_path = NULL;
	char* path = NULL;

	if ((real_path = g_file_read_link("/proc/self/exe", NULL)))
	{
		path = g_path_get_dirname(real_path);

		ln_free(real_path);
	}

	return path;
}


/* WARNING Do not pass these pointers to free */
char* ln_get_exec(char* path)
{
	assert(path);

	return basename(path);
}


char* ln_get_kernel(void)
{
	char* ret = NULL;
	struct utsname buffer;

	if (uname(&buffer))
	{
		ret = g_strjoin(" ",
			buffer.sysname,
			buffer.release,
			buffer.version,
			buffer.machine,
			NULL);
	}

	return ret;
}


char* ln_get_output_cmd(const char* cmd)
{
	assert(cmd);

	int wait_status;
	char* value = NULL;

	g_spawn_command_line_sync(cmd, &value, NULL, &wait_status, NULL);
	g_spawn_check_wait_status(wait_status, NULL);

	if (strlen(value) <= 1)
	{
		ln_free(value);
	}

	return value;
}


char* ln_get_user_cache_path(void)
{
	return _get_xdg_user_path(LN_XDG_USER_CACHE);
}


char* ln_get_user_config_path(void)
{
	return _get_xdg_user_path(LN_XDG_USER_CONFIG);
}


char* ln_get_user_data_path(void)
{
	return _get_xdg_user_path(LN_XDG_USER_DATA);
}


char* ln_get_user_runtime_path(void)
{
	return _get_xdg_user_path(LN_XDG_USER_RUNTIME);
}


char* ln_get_env(const char* suffix_key)
{
	assert(suffix_key);

	char* upper = NULL;
	char* value = NULL;

	if ((upper = g_utf8_strup(suffix_key, (gssize) strlen(suffix_key))))
	{
		char* key = NULL;

		if ((key = g_strconcat("LUNION_", upper, NULL)))
		{
			value = g_strdup(g_getenv(key));
			TRACE("%s=%s", key, value);

			ln_free(key);
		}

		ln_free(upper);
	}

	return value;
}


static void __attribute__((unused)) ln_log_file(const char* logfile)
{
	if (logfile)
	{
		int fd;

		TRACE("Open log file: %s", logfile);
		if ((fd = open(logfile, O_RDWR | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR)) != -1)
		{
			dup2(fd, 1);
			dup2(fd, 2);
			close(fd);
		}
	}
}


void ln_prepend_env(const char* name, const char* value, const char* separator)
{
	assert(name);
	assert(value);

	ln_insert_env(name, value, separator, -1);
}


char* ln_get_hyperlink_file(const char* string)
{
	assert(string);

	char* hyperlink = NULL;

	hyperlink = (char*) calloc((strlen(string) * 2) + 22, sizeof(char));
	if (hyperlink)
	{
		sprintf(hyperlink, "\033]8;;file://%s\033\\%s\033]8;;\033\\", string, string);
	}

	return hyperlink;
}


char* ln_remove_linefeed_to_end_string(char** string)
{
	assert(*string);

	size_t lenght = -1;

	lenght = strlen(*string);
	if ((*string)[lenght - 1] == '\n')
	{
		char* tmp = NULL;

		tmp = g_strndup(*string, lenght - 1);
		ln_free(*string);
		*string = g_strdup(tmp);

		ln_free(tmp);
	}

	return *string;
}


bool ln_reset_env(const char* key, const char* value)
{
	assert(key);

	bool status = false;

	if (value)
	{
		status = g_setenv(key, value, true);
	}
	else
	{
		g_unsetenv(key);
		status = true;
	}

	return status;
}


bool ln_run_process(const char* workdir, char** argv, const bool silent)
{
	assert(argv);

	int wait_status;
	GSpawnFlags flags = G_SPAWN_DEFAULT;
	GError* error = NULL;
	bool status = false;

	_print_argv(argv);
	flags |= G_SPAWN_SEARCH_PATH_FROM_ENVP;

	if (silent)
	{
		flags |= G_SPAWN_STDOUT_TO_DEV_NULL | G_SPAWN_STDERR_TO_DEV_NULL;
	}

	status = g_spawn_sync(workdir,
		argv,
		NULL,
		flags,
		NULL,
		NULL,
		NULL,
		NULL,
		&wait_status,
		&error);

	if (error)
	{
		g_error_free(error);
		error = NULL;
	}

	status &= g_spawn_check_wait_status(wait_status, &error);
	if (error)
	{
		g_error_free(error);
		error = NULL;
	}

	return status;
}
