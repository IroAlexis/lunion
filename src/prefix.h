/*
 * prefix.h
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __PREFIX__
#define __PREFIX__


#include "lib.h"

#include "arch.h"

#include <glib.h>



typedef struct _ln_prefix LnPrefix;


LnPrefix* ln_prefix_create(const char* path);

void ln_prefix_unref(LnPrefix** self);

LnArch ln_prefix_get_arch(const LnPrefix* self);

const char* ln_prefix_get_path(const LnPrefix* self);

bool ln_prefix_setup(const LnPrefix* self, const LnLib* dxvk, const LnLib* vkdp);



#endif
