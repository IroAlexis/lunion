/*
 * prefix.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "prefix.h"

#include "utils/debug.h"
#include "utils/memory.h"
#include "translayer.h"
#include "utils.h"

#include <assert.h>



LN_DEFAULT_DEBUG_CHANNEL(wineprefix);


struct _ln_prefix
{
	const char* path;
	LnLib* lib;
	LnArch arch;
};


static LnArch        _get_windows_arch(const char* path);

static LnLib*        _get_library(const char* path, LnArch arch);

static char*        _get_library_path(const char* path, const char* dirname);

static FILE*         _open_system_file(const char* path);

static char*        _search_arch_to_file(FILE** stream);

static LnPrefix* ln_prefix_new(const char* path, LnLib* lib, LnArch arch);



static LnArch _get_windows_arch(const char* path)
{
	assert(path);

	FILE* stream = NULL;
	LnArch arch = LN_ARCH_NULL;

	if ((stream = _open_system_file(path)))
	{
		char* string = NULL;

		if ((string = _search_arch_to_file(&stream)))
		{
			arch = ln_arch_get_from_string(string);

			ln_free(string);
		}

		fclose(stream);
		stream = NULL;
	}
	else
	{
		ERR("%s: Unable to open system.reg", path);
	}

	return arch;
}


static LnLib* _get_library(const char* path, LnArch arch)
{
	char* lib32 = NULL;
	char* lib64 = NULL;
	LnLib* lib = NULL;

	if (arch == LN_ARCH_64BIT_MODE)
	{
		lib64 = _get_library_path(path, "system32");
		lib32 = _get_library_path(path, "syswow64");
	}
	if (arch == LN_ARCH_32BIT_MODE)
	{
		lib32 = _get_library_path(path, "system32");
	}

	if (lib32 || lib64)
	{
		lib = ln_lib_new(lib32, lib64);
	}

	ln_free(lib32);
	ln_free(lib64);

	return lib;
}


static char* _get_library_path(const char* path, const char* dirname)
{
	assert(path);
	assert(dirname);

	char* lib_path = NULL;

	lib_path = g_build_path(G_DIR_SEPARATOR_S, path, "drive_c", "windows", dirname, NULL);
	if (lib_path && ! g_file_test(lib_path , G_FILE_TEST_IS_DIR))
	{
		ln_free(lib_path);
	}

	return lib_path;
}


static FILE* _open_system_file(const char* path)
{
	char* file = NULL;
	FILE* stream = NULL;

	if ((file = g_build_filename(path, "system.reg", NULL)))
	{
		stream = g_fopen(file, "r");

		ln_free(file);
	}

	return stream;
}


static char* _search_arch_to_file(FILE** stream)
{
	assert(stream);

	char buffer[4096];
	char* value = NULL;

	while (fgets(buffer, 4096, *stream))
	{
		if (g_str_has_prefix(buffer, "#arch"))
		{
			gsize lenght = strlen(buffer);

			value = g_strndup(buffer, lenght - 1);
			break;
		}
	}

	return value;
}


static LnPrefix* ln_prefix_new(const char* path, LnLib* lib, LnArch arch)
{
	assert(path);
	assert(lib);

	LnPrefix* self = NULL;

	if ((self = (LnPrefix*) ln_calloc(1, sizeof(LnPrefix))))
	{
		self->path = path;
		self->lib = lib;
		self->arch = arch;
	}

	return self;
}


/* Factory method */
LnPrefix* ln_prefix_create(const char* path)
{
	assert(path);

	LnArch arch = LN_ARCH_NULL;
	LnPrefix* self = NULL;

	arch = _get_windows_arch(path);
	if (arch != LN_ARCH_NULL)
	{
		LnLib* lib = NULL;

		if ((lib = _get_library(path, arch)))
		{
			self = ln_prefix_new(path, lib, arch);
		}
	}

	if (! self)
	{
		ERR("Failed to create struct LnPrefix");
	}

	return self;
}


void ln_prefix_unref(LnPrefix** self)
{
	if (*self)
	{
		ln_lib_unref(&(*self)->lib);

		ln_free(*self);
	}
}


LnArch ln_prefix_get_arch(const LnPrefix* self)
{
	return self->arch;
}


const char* ln_prefix_get_path(const LnPrefix* self)
{
	return self->path;
}


bool ln_prefix_setup(const LnPrefix* self, const LnLib* dxvk, const LnLib* vkdp)
{
	assert(self);

	bool status = true;

	if (self->lib && dxvk)
	{
		ln_translayer_install(DXVK, dxvk, self->lib);
	}
	if (self->lib && vkdp)
	{
		ln_translayer_install(VKD3DPROTON, vkdp, self->lib);
	}

	return status;
}
