/*
 * program.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "program.h"

#include "utils/debug.h"
#include "utils/memory.h"
#include "utils.h"

#include <assert.h>
#include <sys/stat.h>



LN_DEFAULT_DEBUG_CHANNEL(program);


struct _ln_program
{
	char* exec;
	char* args;
	char* id;
};



static bool _is_valid(const char* path)
{
	assert(path);

	bool status = false;

	status = g_file_test(path, G_FILE_TEST_IS_REGULAR);
	if (status)
	{
		if (g_str_has_suffix(path, ".msi"))
		{
			status = false;
			ERR("%s: No supported file", path);
		}
	}
	else
	{
		ERR("%s: No file", path);
	}

	return status;
}


static LnProgram* ln_program_new(const char* exec, const char* args, const char* id)
{
	assert(exec);

	LnProgram* self = NULL;

	if ((self = (LnProgram*) ln_calloc(1, sizeof(LnProgram))))
	{
		self->exec = g_strdup(exec);
		self->args = g_strdup(args);
		self->id = g_strdup(id);
	}

	return self;
}


LnProgram* ln_program_create(const char* exec, const char* args, const char* id)
{
	assert(exec);

	LnProgram* self = NULL;

	if (_is_valid(exec))
	{
		TRACE("Using executable: %s", exec);
		TRACE("Using arguments: %s", args);
		TRACE("Using program id: %s", id);

		self = ln_program_new(exec, args, id);
	}

	return self;
}


void ln_program_unref(LnProgram** self)
{
	if (*self)
	{
		ln_free((*self)->exec);
		ln_free((*self)->args);
		ln_free((*self)->id);
		ln_free(*self);
	}
}


char* ln_program_get_cache_path(const LnProgram* self)
{
	assert(self);

	char* xdg_cache = NULL;
	char* path = NULL;

	if ((xdg_cache = ln_get_user_cache_path()))
	{
		path = g_build_path(G_DIR_SEPARATOR_S, xdg_cache, self->id, NULL);
		if (path)
		{
			g_mkdir_with_parents(path , 0700);
		}

		ln_free(xdg_cache);
	}

	return path;
}


char* ln_program_get_dirname(const LnProgram* self)
{
	assert(self);

	return g_path_get_dirname(self->exec);
}


char* ln_program_get_command(const LnProgram* self)
{
	assert(self);

	char* filename = NULL;
	char* value = NULL;

	if ((filename = g_path_get_basename(self->exec)))
	{
		if (self->args)
		{
			value = g_strdup_printf("%s,%s", filename, self->args);
		}
		else
		{
			value = g_strdup(filename);
		}

		ln_free(filename);
	}

	return value;
}


const char* ln_program_get_executable(const LnProgram* self)
{
	assert(self);

	return self->exec;
}


const char* ln_program_get_id(const LnProgram* self)
{
	assert(self);

	return self->id;
}
