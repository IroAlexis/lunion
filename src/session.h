/*
 * session.h
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __SESSION__
#define __SESSION__


#include "driver.h"
#include "program.h"
#include "wine.h"

#include <glib.h>



void ln_session_exit(const LnWine* wine);

void ln_session_init(const LnWine* wine, const LnProgram* program, const LnDriver driver);

int ln_session_run(const LnWine* wine, const LnProgram* program);



#endif
