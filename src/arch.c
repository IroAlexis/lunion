/*
 * arch.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "arch.h"

#include "utils/debug.h"

#include <assert.h>
#include <elf.h>
#include <string.h>


#if defined(__LP64__)
#define ElfW(type) Elf64_ ## type
#else
#define ElfW(type) Elf32_ ## type
#endif



LN_DEFAULT_DEBUG_CHANNEL(arch);


static LnArch _elf_ident_to_ln_arch(guchar* e_ident)
{
	LnArch arch = LN_ARCH_NULL;

	if (e_ident[EI_CLASS] == ELFCLASS64)
	{
		arch = LN_ARCH_64BIT_MODE;
	}
	else if (e_ident[EI_CLASS] == ELFCLASS32)
	{
		arch = LN_ARCH_32BIT_MODE;
	}
	else
	{
		ERR("Unknown architecture");
		TRACE("EI_CLASS=%d", e_ident[EI_CLASS]);
	}

	return arch;
}


LnArch ln_arch_get_from_binary_file(char* path)
{
	assert(path);

	FILE* stream = NULL;
	LnArch arch = LN_ARCH_NULL;

	stream = g_fopen(path, "rb");
	if (stream)
	{
		ElfW(Ehdr) header;

		if (fread(&header, sizeof(header), 1, stream) == 1)
		{
			arch = _elf_ident_to_ln_arch(header.e_ident);
		}
		else
		{
			ERR("%s: Unable to get ELF Identification", path);
		}

		fclose(stream);
		stream = NULL;
	}

	return arch;
}


LnArch ln_arch_get_from_string(char* string)
{
	assert(string);

	LnArch arch = LN_ARCH_NULL;

	if (g_regex_match_simple(".*64$", string, G_REGEX_DEFAULT, G_REGEX_MATCH_DEFAULT))
	{
		arch = LN_ARCH_64BIT_MODE;
	}
	else if (g_regex_match_simple("32$|.*86$", string, G_REGEX_DEFAULT, G_REGEX_MATCH_DEFAULT))
	{
		arch = LN_ARCH_32BIT_MODE;
	}
	else
	{
		ERR("Unknown architecture");
		TRACE("string=%s");
	}

	return arch;
}
