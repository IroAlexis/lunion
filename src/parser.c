/*
 * parser.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "parser.h"

#include "utils/debug.h"
#include "utils/memory.h"
#include "utils.h"

#include <assert.h>
#include <glib/gstdio.h>



LN_DEFAULT_DEBUG_CHANNEL(parser);


static char* _build_path(void)
{
	char* path = NULL;

	if (! (path = ln_get_env("config_file")))
	{
		char* dir = NULL;

		if ((dir = ln_get_user_config_path()))
		{
			path = g_build_filename(dir, "config.ini", NULL);

			ln_free(dir);
		}
	}

	return path;
}


static char* _parse_keyfile(LnParser* self, const char* group, const char* key)
{
	assert(self);
	assert(group);
	assert(key);

	char* value = NULL;

	if (g_key_file_has_group(self, group)
		&& g_key_file_has_key(self, group, key, NULL))
	{
		value = g_key_file_get_string(self, group, key, NULL);
		if (value && g_strcmp0(value, "") == 0)
		{
			ln_free(value);
		}
		TRACE("[%s] %s=%s", group, key, value);
	}

	return value;
}


LnDriver ln_parser_get_driver(LnParser* self)
{
	char* value = NULL;
	LnDriver id = LN_DRIVER_NULL;

	/* Expose only in application's group */
	if ((value = ln_parser_get_string(self , "lunion" , "driver")))
	{
		id = ln_driver_get_from_string(value);

		ln_free(value);
	}

	return id;
}


char* ln_parser_get_string(LnParser* self, const char* group, const char* key)
{
	assert(key);

	char* value = NULL;

	if (! (value = ln_get_env(key)) && self)
	{
		value = _parse_keyfile(self, group, key);
		if (! value && g_strcmp0(key, "command") != 0)
		{
			/* Trying to get the key's value in main group */
			value = _parse_keyfile(self, "lunion", key);
		}
	}

	return value;
}


LnWine* ln_parser_get_wine(LnParser* self, const char* group)
{
	char* path = NULL;
	LnWine* wine = NULL;

	if (! (path = ln_parser_get_string(self, group, "wine_path")))
	{
		path = g_find_program_in_path("wineboot");
	}

	if (path)
	{
		wine = ln_wine_create(path);

		ln_free(path);
	}

	return wine;
}


LnParser* ln_parser_create(void)
{
	char* path = NULL;
	LnParser* self = NULL;

	if ((path = _build_path()))
	{
		GError* error = NULL;

		self = g_key_file_new();
		if (g_key_file_load_from_file(self, path, G_KEY_FILE_NONE, &error))
		{
			TRACE("Loading configuration from %s", path);
		}
		else
		{
			ERR("%s", error->message);

			g_key_file_free(self);
			self = NULL;
		}

		if (error)
		{
			g_error_free(error);
			error = NULL;
		}

		ln_free(path);
	}

	return self;
}
