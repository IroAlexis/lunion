/*
 * program.h
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __PROGRAM__
#define __PROGRAM__


#include <glib.h>



typedef struct _ln_program LnProgram;


LnProgram* ln_program_create(const char* command, const char* args, const char* id);

void ln_program_unref(LnProgram** self);

char* ln_program_get_cache_path(const LnProgram* self);

char* ln_program_get_dirname(const LnProgram* self);

char* ln_program_get_command(const LnProgram* self);

const char* ln_program_get_executable(const LnProgram* self);

const char* ln_program_get_id(const LnProgram* self);



#endif
