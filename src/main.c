/*
 * main.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "utils/debug.h"
#include "utils/memory.h"
#include "driver.h"
#include "lib.h"
#include "parser.h"
#include "prefix.h"
#include "program.h"
#include "session.h"
#include "translayer.h"
#include "utils.h"

#include <assert.h>
#include <glib.h>
#include <stdio.h>



LN_DEFAULT_DEBUG_CHANNEL();


static char* _build_wineprefix_path(const char* id)
{
	assert(id);

	char* basedir = NULL;
	char* path = NULL;

	basedir = ln_get_compatdata_path();
	if (basedir && g_mkdir_with_parents(basedir, 0700) == 0)
	{
		path = g_build_path(G_DIR_SEPARATOR_S, basedir, id, NULL);
	}
	else
	{
		ERR("Unable to create path: %s", basedir);
	}

	ln_free(basedir);

	return path;
}


static bool _init_application(int argc, char** argv, char** id, char** exec)
{
	assert(argv);
	assert(*id == NULL);
	assert(*exec == NULL);

	bool status = false;

	if (argc == 2)
	{
		*exec = g_canonicalize_filename(argv[1], NULL);
		if (*exec && g_file_test(*exec, G_FILE_TEST_IS_REGULAR))
		{
			*id = ln_get_env("program_id");
		}
		else
		{
			*id = g_strdup(argv[1]);
			ln_free(*exec);
		}

		status = true;
	}

	return status;
}


static const char* _init_wineprefix(const char* id)
{
	assert(id);

	const char* value = NULL;

	value = g_getenv("WINEPREFIX");
	if (! value)
	{
		char* path = NULL;

		if ((path = _build_wineprefix_path(id)))
		{
			if (g_setenv("WINEPREFIX", path, true))
			{
				value = g_getenv("WINEPREFIX");
			}
			else
			{
				ERR("Unable to initialize WINEPREFIX");
			}

			ln_free(path);
		}
	}

	return value;
}


int main(int argc, char* argv[])
{
	int status = 1;
	char* id = NULL;
	char* exec = NULL;

	if (ln_get_debug_mode())
	{
		ln_debug_set_class_max(LN_DBG_CLASS_TRACE);
	}

	if (_init_application(argc, argv, &id, &exec))
	{
		const char* prefix_path = NULL;
		char* filelink = NULL;
		char* args = NULL;
		LnDriver driver = LN_DRIVER_NULL;
		LnParser* cfg = NULL;
		LnLib* dxvk = NULL;
		LnLib* vkdp = NULL;
		LnProgram* program = NULL;
		LnWine* wine = NULL;

		cfg = ln_parser_create();

		if (! exec)
		{
			exec = ln_parser_get_string(cfg , id, "command");
			if (! exec)
			{
				return status;
			}
		}
		args = ln_parser_get_string(cfg , id , "command_args");

		if (! (program = ln_program_create(exec, args, id)))
		{
			return status;
		}
		ln_free(args);

		if (! (wine = ln_parser_get_wine(cfg, id)))
		{
			return status;
		}

		driver = ln_parser_get_driver(cfg);
		dxvk = ln_translayer_get_dxvk(cfg, id);
		vkdp = ln_translayer_get_vkd3dproton(cfg, id);

		prefix_path = _init_wineprefix(id);
		filelink = ln_get_hyperlink_file(prefix_path);
		INFO("Preparing Windows environment from %s", filelink);

		ln_free(filelink);
		g_key_file_free(cfg);
		cfg = NULL;

		if (ln_wine_make_prefix(wine, NULL))
		{
			LnPrefix* pfx = NULL;

			if ((pfx = ln_prefix_create(prefix_path)))
			{
				ln_prefix_setup(pfx, dxvk, vkdp);

				ln_session_init(wine, program, driver);

				INFO("Starting...");
				status = ln_session_run(wine, program);

				ln_session_exit(wine);
				ln_prefix_unref(&pfx);
			}
		}

		ln_lib_unref(&dxvk);
		ln_lib_unref(&vkdp);
		ln_program_unref(&program);
		ln_wine_unref(&wine);

		ln_free(id);
		ln_free(exec);
	}

	return status;
}
